`i.nightlights.intercalibration` is a GRASS-GIS module performing
inter-satellite calibration on DMSP-OLS nighttime lights time series. Based on
"well known" empirical regression models, it calibrates average visible band
Digital Number values.

*Note*, the module is still under testing. Eventually minor, but important,
changes might be applied to the intercalibration process.

## Notes from a review paper
----------------------------

- "Several methods were proposed to overcome the lack of inter-satellite 
calibration. These include the invariant region and the quadratic regression 
method proposed by Elvidge et al. [23], the second-order regression and optimal 
threshold method proposed by Liu et al. [24], and a power-law regression 
method proposed by Wu et al. [25]. Although studies based on these calibration 
methods showed performance improvement after the rectification [24,25], the 
assumption that the nighttime light remains stableover time in a particular 
area requires a careful choice of the invariant region manually." [Huang 2014]

- references above are: [23]: [Elvidge 2009] [24]: [Liu 2012], [25]: [Wu 2013]

## Overview

```
   +----------------------------------------------------------------------+
   |                                                                      |
   |          +-----------------+                                         |
   | DN  +--> |Calibration Model| +-->  Calibrated DN                     |
   |          +---^-------------+            ^                            |
   |              |                          |                            |
   |              |             +--Evaluation+Methods-------------------+ |
   |              |             |                                       | |
   |              |             | ?                                     | |
   |              |             |                                       | |
   |              |             +---------------------------------------+ |
   |              |                                                       |
   | +--Regression+Models-----------------------------------------------+ |
   | |                                                                  | |
   | |  Elvidge, 2009/2014:  DNc = C0 + C1×DN + C2×DNv2                 | |
   | |                                                                  | |
   | |  Liu, 2012:  based on Elvidge's model + optimal threshold method | |
   | |                                                                  | |
   | |  Wu, 2014:            DNc + 1 = a×(DN + 1)^b                     | |
   | |                                                                  | |
   | |  Others?                                                         | |
   | |                                                                  | |
   | +------------------------------------------------------------------+ |
   |                                                                      |
   +----------------------------------------------------------------------+
```

The following plot compares well known calibration models.

![nightlights_intercalibration_models_comparison](https://gitlab.com/NikosAlexandris/i.nightlights.intercalibration/uploads/525f8a34aa99b0d44a6f3efc65820be5/nightlights_intercalibration_models_comparison.png)

Read the https://gitlab.com/NikosAlexandris/i.nightlights.intercalibration/blob/master/documentation_R.pdf for details (source: https://gitlab.com/NikosAlexandris/i.nightlights.intercalibration/blob/master/documentation_R.lyx)

Installation
============

## Requirements


see [GRASS Addons SVN repository, README file, Installation - Code 
Compilation](https://svn.osgeo.org/grass/grass-addons/README)

## Steps

Making the script `i.nightlights.intercalibration` available from within any GRASS-GIS ver. 7 
session, may be done via the following steps:

1.  launch a GRASS-GIS’ ver. 7 session

2.  navigate into the script’s source directory

3.  execute `make MODULE_TOPDIR=$GISBASE`

Usage
=====

After installation, from within a GRASS-GIS session, see help details via 
`i.nightlights.intercalibration --help` -- also provided here:

```
Description:
 Performs inter-satellite calibration on DMSP-OLS Nighttime Lights Time Series

Keywords:
 imagery, inter-satellite, calibration, nighttime lights, time series, DMSP-OLS

Usage:
 i.nightlights.intercalibration [-ciegxznt] image=name[,name,...]
   suffix=suffix model=author [--overwrite] [--help] [--verbose]
   [--quiet] [--ui]

Flags:
  -c   Print out citation for selected calibration model
  -i   Print out calibration equations
  -e   Evaluation based on the Normalised Difference Index
  -g   Print in shell script style (currently only NDI via -e)
  -x   Match computational region to extent of input image
  -z   Exclude zero values from the analysis (retain zero cells in output)
  -n   Exclude zero values from the analysis (set zero cells to NULL in output)
  -t   Do not try to transfer timestamps (for input without timestamp)

Parameters:
   image   Clean average DMSP-OLS visible band digital number image(s)
  suffix   output file(s) suffix
           default: c
   model   Calibration model
           options: elvidge2009,elvidge2014,liu2012,wu2013
           default: elvidge2014
```
## Example

Given all maps are imported in GRASS' data base, which are:

```
g.list rast pattern=F* sep=,

F101992,F101993,F101994,F121994,F121995,F121996,F121997,F121998,F121999,F141997,
F141998,F141999,F142000,F142001,F142002,F142003,F152000,F152001,F152002,F152003,
F152004,F152005,F152006,F152007,F162004,F162005,F162006,F162007,F162008,
F162009,F182010,F182011,F182012
```

the "default" inter-calibration, based on [Elvidge 2014] can be performed as:

```
i.nightlights.intercalibration image=`g.list rast pattern=F* sep=,`
```

## Remarks

- The calibration models do *not* include regression coefficients for all 
of the yearly products. In such a case, the module will fail and inform with an 
error message like:

```
i.nightlights.intercalibration image=`g.list rast pattern=F?????? sep=,` model=liu2012 --v

...
ValueError: The selected model does not know about this combination of Satellite 
+ Year!
```

Implementation notes
====================

- First commit on Thu Feb 26 16:07:38 2015 +0200

- Working state reached on Wed Mar 4 18:57:59 2015 +0200


## To Do

### in general

- improve missing key handling and error reporting

- code deduplication

- test -- will it compile in other systems?

### in `i.nightlights.intercalibration.py`

- use `*args` or `**kwargs` where appropriate

in `calibration_models.py`:

- improve checks for missing combinations of Satellite + Year in models

- separate test_function from this "module"

### another module?

- Accuracy assessment of inter-calibrated nighttime lights time series [Wu
  2013]: TLI= Σi DNi × Ci where DNi is the grey value of i-level pixels and Ci
  is the number of i-level pixels

References
==========

### Review paper

- Application of DMSP⁄OLS Nighttime Light Images A Meta-Analysis and a
  Systematic Literature Review  [Huang 2014]


### Empirical second order regression model by Elvidge, 2009

- Elvidge's regression model: `Y = C0 + C1*X + C2*X^2`

- Estimating Land Development Time Lags in China Using DMSP⁄OLS Nighttime Light
  Image  [Zhang 2015]

- National Trends in Satellite-Observed Lighting 1992–2012  [Elvidge 2014]

- Comparative Estimation of Urban Development in China’s Cities Using
  Socioeconomic and DMSP⁄OLS Night Light Data  [Fan 2014]

- The Integrated Use of DMSP-OLS Nighttime Light and MODIS Data for Monitoring
  Large-Scale Impervious Surface Dynamics A Case Study in the Yangtze River
  Delta  [Shao & Liu 2014]

- Characterizing Spatio-Temporal Dynamics of Urbanization in China Using Time
  Series of DMSP⁄OLS Night Light Data  [Xu 2014]

- Night on Earth Mapping decadal changes of anthropogenic night light in Asia
  [Small & Elvidge 2013]


### Second order regression model & optimal threshold method by Liu, 2012

- Modeling In-Use Steel Stock in China's Buildings and Civil Engineering
  Infrastructure Using Time-Series of DMSP⁄OLS Nighttime Lights [Liang 2014]

- Dynamics of Urbanization Levels in China from 1992 to 2012 Perspective from
  DMSP⁄OLS Nighttime Light Data] [?]


### Non-linear, power regression model

- Intercalibration of DMSP-OLS night-time light data by the invariant region
  method [Wu 2013]


Ευχαριστώ
=========

- Demetris Stathakis
- Ioannis Faraslis
- Pietro Zambelli
- StackExchange contributors
  - <https://docs.python.org/2/library/csv.html#csv.reader>
  - <http://stackoverflow.com/q/1747817/1172302>
  - <http://stackoverflow.com/q/14091387/1172302>
  - <http://stackoverflow.com/a/10664874/1172302>
  - <http://stackoverflow.com/q/3294889/1172302>
  - <http://stackoverflow.com/q/20062827/1172302>
  - <http://stackoverflow.com/a/8748774/1172302>
  - <http://stackoverflow.com/a/5036775/1172302>
  - <http://stackoverflow.com/a/12382738/1172302>
  - <http://openbookproject.net/thinkcs/python/english3e/tuples.html>
  - <http://stackoverflow.com/a/1746549/1172302>
