# -*- coding: utf-8 -*-
"""
@author: nik | Created on Wed Mar 11 18:09:26 2015
"""

CITATIONS = {
    'ELVIDGE2009':
    ('Elvidge, Christopher D., Daniel Ziskin, '
     'Kimberly E. Baugh, Benjamin T. Tuttle, Tilottama Ghosh, Dee W. Pack, '
     'Edward H. Erwin, and Mikhail Zhizhin. “A Fifteen Year Record of '
     'Global Natural Gas Flaring Derived from Satellite Data.” Energies 2, '
     'no. 3 (August 7, 2009): 595–622.'),
    'ELVIDGE2014':
    ('Elvidge, Christopher D., Feng-Chi Hsu, '
     'Kimberly E. Baugh, and Tilottama Ghosh. “National Trends in '
     'Satellite-Observed Lighting.” Global Urban Monitoring and '
     'Assessment Through Earth Observation (2014): 97.'),
    'LIU2012':
    ('Liu, Zhifeng, Chunyang He, Qiaofeng Zhang, '
     'Qingxu Huang, and Yang Yang. '
     '"Extracting the Dynamics of Urban Expansion in China Using DMSP-OLS '
     'Nighttime Light Data from 1992 to 2008." '
     'Landscape and Urban Planning 106, no. 1 (May 2012): 62-72.'),
    'WU2013':
    ('Jiansheng Wu, Shengbin He, Jian Peng, Weifeng Li '
     '& Xiaohong Zhong (2013). '
     'Intercalibration of DMSP-OLS night-time light data by the invariant '
     'region method, International Journal of Remote Sensing, 34:20, '
     '7356-7368. DOI:10.1080/01431161.2013.820365')}

# reusable & stand-alone
if __name__ == "__main__":
    print ('Citations for calibration models for DMSP-OLS NightTime '
           'Lights Time Series')
    print CITATIONS
