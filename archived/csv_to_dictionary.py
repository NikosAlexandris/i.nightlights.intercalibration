# -*- coding: utf-8 -*-
"""
@author: nik | Created on Fri Feb 27 02:11:09 2015

ToImprove:

- retrieving coefficients?
- building nested dictionary(/ies) by using the .get method (advised by PZ)
- Ask/Accept user-defined csv filename(s) to operate on?
- Automatically detect csv files in directory, and loop over
- Build a test?
- exporting of dictionary to ASCII file
"""

# globals -------------------------------------------------------------------
COEFFICIENTS = {}
csvfiles = ['elvidge_2014.csv', 'elvidge_2009.csv', 'wu_2013.csv', 'liu_2012.csv']

# required librairies
import os
import csv

# helper functions ----------------------------------------------------------
def clean_header(header):
    """
    Remove unwanted "columns from the input csv header
    """
    header_clean = list(header)
 
    if "Satellite" in header:
        header_clean.remove('Satellite')
    if 'Year' in header:
        header_clean.remove('Year')
    if 'Number' in header:
        header_clean.remove('Number')

    return header_clean

def name_after_filename(filename):
    """
    Retrieve a (csv) file's basename
    """
    dictionary_name = filename.strip().split('.')[0].translate(None, '_').upper()
    return dictionary_name

def get_coefficients_from_csvfile(csvfile):
    """
    Get the calibration model coefficients from a "properly" formatted csv file
    """

    dictionary = {}
    dictionary_filenames = {}
#    dictionary_satellites = {}
    dictionary_years = {}
    
    reader = csv.DictReader(open(csvfile, mode='r'))  # import csv    
    header = clean_header(list(reader.fieldnames))  # reject irrelevant fields

    key_filename = name_after_filename(csvfile)

    # loop over the input csv file's rows
    for row in reader:

    # final output should be in form of:
    # { key_filename : {key_satellite : {key_year : coefficients}}}

# ---------------------------------------------------------------------------
# idea by PZ:
 # 1. split the row in sat, year, data
     # def split(row): return row[0], row[1], np.array(row[2:], dtype=float)
         # then
     # def unpackcsv(csvfile): [stuff] yield sat, year, data

 # 2. get the sat dict or create a new one with get
 # 3. add to this sat dict the year data
 # 4. and then, add the sat dict back to parent dict (your result)
# ---------------------------------------------------------------------------

#        key_satellite = row['Satellite']  # keys for initial dictionary
        key_satellite = row.pop("Satellite")  #;print key_satellite

#        key_year = row['Year']  # keys for initial dictionary
        key_year = row.pop("Year")  #;print key_year

#        coefficients = tuple([float(row[column]) for column in header]) # get
        coefficients = tuple([float(row.pop(column)) for column in header])  #;print coefficients

        # check if 'Year' does NOT exist as dictionary key
        if key_year not in dictionary_years.keys():  # and add as new key
            dictionary_years.update( {key_year : coefficients } )
    
        else:  # else, add coefficients to existing 'Year'
            dictionary_years = {key_year : coefficients }
        
        # build dictionaries
        dictionary_filenames.update({key_satellite : dictionary_years})
    
    # add to master dict
    dictionary[key_filename] = dictionary_filenames
    
    return dictionary


def export_to_ascii(dictionary, filename, separator):
    """Exporting ... to an ASCII file"""
         
    # convert dictionary to string
    dictionary = str(dictionary)

    # define filename
    filename += '.py'
    
    # don't overwrite!
    if not os.path.exists(filename):
        
        # structure informative message
        msg = '> Exporting python dictionary as is...'
        print msg
    
        # open, write and close file
        asciif = open(filename, 'w')
        asciif.write(dictionary)
        asciif.close()

    else:
        print '{f} already exists!'.format(f = filename)


# main program --------------------------------------------------------------
def main():
    """
    Main program...
    """
# <<<<<<< HEAD
# 
#     # loop over csv files of interest
#     for csvfile in csvfiles:
#         
#         # get coefficients from csv into a temporary dictionary
#         tmp = get_coefficients_from_csvfile(csvfile)
#         
#         # define a suited name for it
#         dname = name_after_filename(csvfile)
#         
#         # use...
#         sub_dictionary = tmp[dname] 
#         COEFFICIENTS[dname] = COEFFICIENTS.get(dname, sub_dictionary)
#         
#     # export, hardcoded filename
#     asciifile = 'intercalibration_coefficients'
#     export_to_ascii(COEFFICIENTS, asciifile, separator='|')
# 
# =======
    for csvfile in csvfiles:
        get_coefficients_from_csvfile(csvfile)

# >>>>>>> uniclass
if __name__ == "__main__":
    main()
