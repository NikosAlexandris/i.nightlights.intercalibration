#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass tufte-handout
\use_default_options true
\begin_modules
knitr
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_title "Comparison of Inter-Calibration models for DMSP-OLS NightTime Lights Time Series"
\pdf_author "Nikos Alexandris"
\pdf_subject "Comparison of Intercalibration models"
\pdf_keywords "regression, inter-satellite calibration, inter-calibration, nighttime lights"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\listings_params "breaklines=true"
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout

<<setup, include=FALSE, cache=FALSE>>=
\end_layout

\begin_layout Plain Layout

library(knitr)
\end_layout

\begin_layout Plain Layout

opts_chunk$set(fig.path = 'figure/listings-')
\end_layout

\begin_layout Plain Layout

options(formatR.arrow = TRUE)
\end_layout

\begin_layout Plain Layout

render_listings()
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Title
Comparison of Inter-Calibration models for DMSP-OLS NightTime Lights Time
 Series
\end_layout

\begin_layout Author
Nikos Alexandris
\end_layout

\begin_layout Abstract
Short tutorial plotting inter-satellite calibrated sum of night-time lights
 time series based on different models using 
\emph on
R
\end_layout

\begin_layout Section
Raw data
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Section
Inter-satellite calibration
\end_layout

\begin_layout Standard
The inter-satellite calibration on DMSP-OLS nighttime lights time series
 was performed using 
\begin_inset CommandInset href
LatexCommand href
name "i.nightlights.intercalibration"
target "https://gitlab.com/NikosAlexandris/i.nightlights.intercalibration"

\end_inset

, a 
\begin_inset CommandInset href
LatexCommand href
name "GRASS-GIS"
target "https://grass.osgeo.org/"

\end_inset

 module.
 Based on "well known" empirical regression models, the module calibrates
 average visible band Digital Number values.
\end_layout

\begin_layout Section
Import in R
\end_layout

\begin_layout Standard
First, and if required, set the working directory
\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

# set working directory, as required
\end_layout

\begin_layout Plain Layout

setwd("/geo/grassdb/mollweide/nightlights_intercalibration")
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

# confirm
\end_layout

\begin_layout Plain Layout

getwd()
\end_layout

\begin_layout Plain Layout

dir()
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Import all csv files of interest.
 In order get the 
\emph on
datetime 
\emph default
columns easily...
 ...should 
\emph on
not 
\emph default
be imported as a Factor
\begin_inset Flex Sidenote
status open

\begin_layout Plain Layout
Make a function to read in all csv files in one step!
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

# read data from csv files
\end_layout

\begin_layout Plain Layout

ntl <- read.csv("univar_ntl", sep="|", stringsAsFactors=F)
\end_layout

\begin_layout Plain Layout

elvidge <- read.csv("univar_elvidge", sep="|", stringsAsFactors=F)
\end_layout

\begin_layout Plain Layout

liu <- read.csv("univar_liu", sep="|", stringsAsFactors=F)
\end_layout

\begin_layout Plain Layout

wu <- read.csv("univar_wu", sep="|", stringsAsFactors=F)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Check the structure of the objects
\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

# check structure & summary of one object, repeat likewise
\end_layout

\begin_layout Plain Layout

str(ntl)
\end_layout

\begin_layout Plain Layout

summary(ntl)
\end_layout

\end_inset


\end_layout

\begin_layout Section
Some pre-processing
\end_layout

\begin_layout Standard
The columns 
\emph on
start
\emph default
 and 
\emph on
end 
\emph default
are imported as being of class 
\emph on
character
\emph default
.
 Casting
\begin_inset Flex Marginnote
status open

\begin_layout Plain Layout
Make a function for the conversion
\end_layout

\end_inset

 their content to 
\emph on
datetime 
\emph default
may be done as follows:
\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

ntl[,'start'] <- as.POSIXct(ntl[,'start'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\begin_layout Plain Layout

ntl[,'end'] <- as.POSIXct(ntl[,'end'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

elvidge[,'start'] <- as.POSIXct(elvidge[,'start'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\begin_layout Plain Layout

elvidge[,'end'] <- as.POSIXct(elvidge[,'end'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

liu[,'start'] <- as.POSIXct(liu[,'start'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\begin_layout Plain Layout

liu[,'end'] <- as.POSIXct(liu[,'end'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

wu[,'start'] <- as.POSIXct(wu[,'start'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\begin_layout Plain Layout

wu[,'end'] <- as.POSIXct(wu[,'end'], format="%Y-%m-%d %H:%M:%S")
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Next, confirm the casting
\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

str(ntl)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

# or shorter
\end_layout

\begin_layout Plain Layout

class(ntl[,'start'])
\end_layout

\end_inset


\end_layout

\begin_layout Section
Visual comparison of time series
\end_layout

\begin_layout Standard
The data are:
\end_layout

\begin_layout Itemize
x= yearly sum of lights in (a) region
\end_layout

\begin_layout Itemize
y= years
\end_layout

\begin_layout Standard
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

x <- ntl[,'start']
\end_layout

\begin_layout Plain Layout

y <- ntl[,'sum']
\end_layout

\begin_layout Plain Layout

x.elvidge <- elvidge[,'start']
\end_layout

\begin_layout Plain Layout

y.elvidge <- elvidge[,'sum']
\end_layout

\begin_layout Plain Layout

x.liu <- liu[,'start']
\end_layout

\begin_layout Plain Layout

y.liu <- liu[,'sum']
\end_layout

\begin_layout Plain Layout

x.wu <- wu[,'start']
\end_layout

\begin_layout Plain Layout

y.wu <- wu[,'sum']
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Experimenting/Checking for normality?
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide true
sideways false
status open

\begin_layout Plain Layout
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

\begin_inset Argument 1
status open

\begin_layout Plain Layout
QQPlot, keep.source=F, fig.show='hold', out.width='.49
\backslash

\backslash
linewidth', echo=FALSE, eps=T, pdf=T, dev='pdf'
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

require(car, quietly=TRUE)
\end_layout

\begin_layout Plain Layout

avg <- ntl[,'mean']
\end_layout

\begin_layout Plain Layout

s <- ntl[,'stddev']
\end_layout

\begin_layout Plain Layout

qqPlot(y, "norm", mean=avg, sd=s, main = "Normality of...", ylab = "DMSP-OLS
 Night Time Lights Time, Sample Quantiles")
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

avg.elvidge <- elvidge[,'mean']
\end_layout

\begin_layout Plain Layout

s.elvidge <- elvidge[,'stddev']
\end_layout

\begin_layout Plain Layout

qqPlot(y.elvidge, "norm", mean=avg.elvidge, sd=s.elvidge, ylab = "Elvidge,
 2014")
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

avg.liu <- liu[,'mean']
\end_layout

\begin_layout Plain Layout

s.liu  <- liu[,'stddev']
\end_layout

\begin_layout Plain Layout

qqPlot(y.liu, "norm", mean=avg.liu, sd=s.liu, ylab = "[Liu 2012]")
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

avg.wu <- wu[,'mean']
\end_layout

\begin_layout Plain Layout

s.wu  <- wu[,'stddev']
\end_layout

\begin_layout Plain Layout

qqPlot(y.wu, "norm", mean=avg.wu, sd=s.wu, ylab = "Wu 2013")
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:QQ-Plots"

\end_inset

Q-Q Plots | Yearly Sums of DMSP-OLS Night Time Lights Time Series
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Full Width
Some plotting
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide true
sideways false
status open

\begin_layout Plain Layout
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

\begin_inset Argument 1
status open

\begin_layout Plain Layout
plot-one, fig=TRUE, fig.show='hold', fig.width=10, fig.height=5, out.width='
\backslash

\backslash
linewidth', keep.source=F, dev = 'pdf'
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

require(sfsmisc)
\end_layout

\begin_layout Plain Layout

y.minrange <- min( range(y), range(y.elvidge), range(y.liu), range(y.wu))
\end_layout

\begin_layout Plain Layout

y.maxrange <- max( range(y), range(y.elvidge), range(y.liu), range(y.wu))
\end_layout

\begin_layout Plain Layout

par(mar=c(2.1, 5.1, 1, 0.5))
\end_layout

\begin_layout Plain Layout

plot(x, y, ylim = c(y.minrange, y.maxrange), col="darkgrey", xlab = "", ylab
 = "", yaxt = "n", type="b", lwd=1.5)
\end_layout

\begin_layout Plain Layout

mtext("Sum of Lights", side=2, line=4)
\end_layout

\begin_layout Plain Layout

lines(x.elvidge, y.elvidge, yaxt = "n", xaxt = "n", col="red", lwd=.5, type="b",
 pch = 10)
\end_layout

\begin_layout Plain Layout

lines(x.liu, y.liu, yaxt = "n", xaxt = "n", col="darkgreen", lwd=.5, type="b",
 pch=12)
\end_layout

\begin_layout Plain Layout

lines(x.wu, y.wu, yaxt = "n", xaxt = "n", col="blue", lwd=.5, type="b", pch=14)
\end_layout

\begin_layout Plain Layout

legend("topleft", legend = c("NTL", "Elvidge", "Liu", "Wu"), lty = c(1,
 1, 1, 1), col = c ("black", "red", "green", "blue"))
\end_layout

\begin_layout Plain Layout

eaxis(2)
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Sum of lights
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
\start_of_appendix
To Do
\end_layout

\begin_layout Standard
Figure out breaklines option for 
\emph on
listings 
\emph default
in 
\emph on
knitr
\emph default
.
\end_layout

\end_body
\end_document
